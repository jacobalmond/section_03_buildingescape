// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorEvent.h"
#include "GameFramework/Actor.h"
#include "Grabber.h"

#define OUT

// Sets default values for this component's properties
UDoorEvent::UDoorEvent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDoorEvent::BeginPlay()
{
	Super::BeginPlay();

	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing trigger volume for pressure plate!"), *GetOwner()->GetName());
		return;
	}

	Owner = GetOwner(); // Find and set the owning actor	
		
}

// Called every frame
void UDoorEvent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Poll The Trigger Volume
	if (!PressurePlate) {return;}

	if (GetTotalMassofActorsOnPlate() > TriggerMass)
	{
		if (!DoorIsOpen)
		{
			OnOpen.Broadcast();
			DoorIsOpen = true;
		}
		
	}

	else
	{
		if (DoorIsOpen)
		{
			OnClose.Broadcast();
			DoorIsOpen = false;
		}
		
	}
	
}

float UDoorEvent::GetTotalMassofActorsOnPlate()
{
	float TotalMass = 0.f;

	// Find all the overlapping actors

	TArray<AActor*> OverlappingActors;
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);

	// Iterate through them adding their masses
	for (const auto& Actor : OverlappingActors)
	{
		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();	
		UE_LOG(LogTemp, Warning, TEXT("%s on pressure plate"), *Actor->GetName());
	}

	return TotalMass;
}

